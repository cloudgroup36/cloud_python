import couchdb
import couchdb.design
import json
from util.read_properties import getProperties

COUCH_SERVER = 'http://couchdb:cloud@localhost:5984/'


class TweetStore(object):
    __dbname = None
        def __init__(self,dbname,url=COUCH_SERVER):
            try:
                self.server = couchdb.Server(url=url)
                    self.__dbname = dbname
                        self.db = self.server.create(dbname)
                        self._create_views()
                except couchdb.http.PreconditionFailed:
                    self.db = self.server[dbname]

    def _create_views(self):
        count_map = 'function(doc){emit(doc.id,doc)}'
            count_reduce = 'function(keys,values){return sum(values)}'
                view = couchdb.design.ViewDefinition(__dbname,'get_tweets',count_map,reduce_fun=count_reduce)
                view.sync(self.db)

if __name__ == '__main__':
    db_properties = getProperties("./db.properties")
    COUCH_DB = db_properties['db_database']
    storage = TweetStore(COUCH_DB)

import json
import couchdb
import threading
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import StreamListener
from tweepy import Stream

#writes tweets to a json file 
class StdOutListenerToJSON(StreamListener):

	__filePath=""

	def __init__(self,filePath):
		self.__filePath = filePath

	def on_data(self,data):
		try:
			# tweet = json.loads(data)
			with open(self.__filePath,'a') as f:
				# json.dump(tweet,f)
				f.write(data)
		except BaseException:
			print("error on processing tweets")
			pass
		return True

	def on_error(self,status):
		print(status)

#writes tweets to a couchdb database
class StdOutListenerToDB(StreamListener,threading.Thread):

	__db = None
	__docs = None
	__batch = 0
	__lock = None
    	#maybe can be declared in .properties file
	__THRESHOLD = 20

	def __init__(self, db, lock):
		self.__db = db
		self.__docs = []
		__batch = 0
		self.__lock = lock

	def on_data(self,data):
		doc = json.loads(data)
		self.__lock.acquire()
		try:
			if doc['id_str'] not in self.__db:
				doc['_id'] = doc['id_str']
				self.__batch += 1
				self.__docs.append(doc)
	        		#self.__db.save(doc)
	        		#using update() function is more effecient
				if self.__batch == self.__THRESHOLD:
					self.__db.update(self.__docs)
					self.__docs = []
					self.__batch = 0
		finally:
			self.__lock.release()
		return True

	def on_error(self,status):
		print(status)

#use stream.filter() function with 'locations' or 'track' params
def processTweets(filter,stream):
	my_location = filter.get('locations',None)
	my_track = filter.get('track',None)
	if my_location is None and my_track is None:
		stream.filter()
	elif my_location is not None and my_track is not None:
		stream.filter(locations = my_location,track = my_track)
	elif my_location is None and my_track is not None:
		stream.filter(track = my_track)
	elif my_location is not None and my_track is None:
		stream.filter(locations = my_location)
	else:
		print("error on stream.filter")
		return

#get stream for processTweets func
def getStream(key_properties, stream_listener):
	auth = OAuthHandler(key_properties['consumer_key'],key_properties['consumer_secret'])
	auth.set_access_token(key_properties['access_token'],key_properties['access_token_secret'])
	stream = Stream(auth,stream_listener)
	return stream


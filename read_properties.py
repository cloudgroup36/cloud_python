#str list to float/int list
def getList(eval,dict):
  dict = map(eval,dict)
  temp = []
  for item in dict:
    temp.append(item)
  return temp

class Properties:
  fileName = ''
  def __init__(self, fileName):
    self.fileName = fileName

  def getProperties(self):   
    try:
      pro_file = open(self.fileName, 'r')
      properties = {}
      for line in pro_file:
        if line.find('=') > 0:
          strs = line.replace('\n', '').split('=') 
          if strs[1].find(',') > 0:
            properties[strs[0]] = strs[1].split(',')
          else:
            properties[strs[0]] = strs[1]
          if strs[0] == "locations":      
            properties[strs[0]] = getList(float,properties[strs[0]])
    except Exception as e:
      print(e+" error processing properties.")
      return None
    else:
      return properties
    finally:
      pro_file.close()


#export function, import when read .properties files.
#param: 
# filePath: the absolute file path or relative file path (starts with ./) of .properties file
#return:
# dict of properties
def getProperties(filePath):
  fileName = filePath.split('/')[-1]
  fileType = fileName.split('.')[-1]
  if fileType == "properties":
    p = Properties(filePath)
    properties = p.getProperties()
    return properties
  else:
    print(filePath+" error read properties.")
    return None


#test
if __name__ == '__main__':
  properties = getProperties("./key-example.properties")
  print(properties)
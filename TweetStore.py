import couchdb
import couchdb.design
import json


COUCH_SERVER = 'http://couchdb:cloud@localhost:5984/'


class TweetStore(object):
	def __init__(self, dbname, url=COUCH_SERVER):
		try:
			self.server = couchdb.Server(url=url)
			self.db = self.server.create(dbname)
			self._create_views()
		except couchdb.http.PreconditionFailed:
			self.db = self.server[dbname]

	def _create_views(self):
		count_map = 'function(doc) { emit(doc.id, doc); }'
		count_reduce = 'function(keys, values) { return sum(values); }'
		view = couchdb.design.ViewDefinition('twitter', 'count_tweets', count_map, reduce_fun=count_reduce)
		view.sync(self.db)

		get_tweets = 'function(doc) { emit(("0000000000000000000"+doc.id).slice(-19), doc); }'
		view = couchdb.design.ViewDefinition('twitter', 'get_tweets', get_tweets)
		view.sync(self.db)

	def save_tweet(self, tw):
		if tw['id_str']  not in self.db:
			tw['_id'] = tw['id_str']
			self.db.save(tw)

	def count_tweets(self):
		for doc in self.db.view('twitter/count_tweets'):
			return doc.value

	def get_tweets(self):
		return self.db.view('twitter/get_tweets')

if __name__ == '__main__':
	COUCH_DATABASE = 'tweets'

	"""
	API_KEY = XXX
	API_SECRET = XXX
	ACCESS_TOKEN = XXX
	ACCESS_TOKEN_SECRET = XXX
	I"""
	storage = TweetStore(COUCH_DATABASE)

	#api = TwitterAPI(API_KEY, API_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

	filename = '/Users/xutianyu/Python/Node1_tweets_AU_1.json'
	f = open(filename,'r')
	count = 0

	for line in f:
		line = line.strip(",\n")
		line = line.strip("\n")
		try:
			if line != "[" and line != "]" and isinstance(json.loads(line),dict):
				line_dic = json.loads(line)
				storage.save_tweet(line_dic)
		except ValueError:
			count += 1






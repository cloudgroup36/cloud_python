from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from util.read_properties import getProperties
import time
import json


class StdoutListener(StreamListener):
    def on_data(self,data):
        try:
            data = json.loads(data)
            print(type(data))
            print(data)
            print(data['id_str'])
            #savefile=open("./csv/tweets.csv","a")
            #savefile.write(data)
            #savefile.write('\n')
            #savefile.close()
            return True
        except BaseException as e:
            print ("Failed on Data",str(e))
            time.sleep(5)
    def on_error(self,status):
        print(status)

def main():
    properties = getProperties("./key.properties")
    I=StdoutListener()
    auth = OAuthHandler(properties['consumer_key'],properties['consumer_secret'])
    auth.set_access_token(properties['access_token'],properties['access_token_secret'])
    stream = Stream(auth,I)
    stream.filter(track=['food','delicious','restaurant','creativity'])

if __name__ == "__main__":
    main()

import json
import pandas as pd
import matplotlib.pyplot as plt


tweets_data_path="./csv/tweets.csv"
tweets_data=[]

tweets_file=open(tweets_data_path,"r")

for line in tweets_file:
    try:
        tweet = json.loads(line)
        tweets_data.append(tweet)
    except:
        continue


tweets=pd.DataFrame()
tweets["text"] = map(lambda tweet:tweet['text'],tweets_data)
tweets["lang"] = map(lambda tweet:tweet['lang'],tweets_data)


tweets_by_lang=tweets['lang'].value_counts()

fig,ax = plt.subplots()
ax.tick_params(axis='x',labelsize=15)
ax.tick_params(axis='y',labelsize=10)
ax.set_xlabel('lang',fontsize=15)
ax.set_ylabel('Num of tweets',fontsize=15)
ax.set_title('Top 3 langs in food',fontsize=20,fontweight='bold')
tweets_by_lang[:3].plot(ax=ax,kind='bar',color='blue')
plt.show()

import couchdb
import threading
from read_properties import getProperties
from customized_tweepy import StdOutListenerToJSON
from customized_tweepy import StdOutListenerToDB
from customized_tweepy import processTweets
from customized_tweepy import getStream
def twitterHarvester(threadNum,key_properties,db,filter_properties):
	if threadNum == 0:
		print "error on keys"
		return
	else:
		lock = threading.Lock()
		for i in range(threadNum):
			key_property = {}
			pos_index = str(i+1)
			consumer_key = "consumer_key"+pos_index
			consumber_secret = "consumer_secret"+pos_index
			access_token = "access_token"+pos_index
			access_token_secret = "access_token_secret"+pos_index
			key_property['consumer_key'] = key_properties[consumer_key]
			key_property['consumer_secret'] = key_properties[consumber_secret]
			key_property['access_token'] = key_properties[access_token]
			key_property['access_token_secret'] = key_properties[access_token_secret]	
			l = StdOutListenerToDB(db,lock)
			stream = getStream(key_property,l)

			t = threading.Thread(target = processTweets, args = (filter_properties,stream,))
			t.start()
		
	
def main():
	#optional: get db properties, establish connections with couchdb
	db_properties = getProperties("./db.properties")
	couch = couchdb.Server(db_properties['db_path'])
	db = couch[db_properties['db_database']]

	#get consumer key properties, get access to twitter api
	key_properties = getProperties("./key.properties")
	threadNum = int(len(key_properties)/4) if len(key_properties)%4==0 else 0
	filter_properties = getProperties("./filter.properties")
	#StdOutListenerToJson writes tweets to json file
	#StdOutListenerToDB writes tweets to couchdb
	# l = StdOutListenerToJSON("./json/Node.json")
	twitterHarvester(threadNum, key_properties, db, filter_properties)

if __name__ == '__main__':
	main()








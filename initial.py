import boto
import os, errno
import time
import argparse
from boto.ec2.regioninfo import RegionInfo
from pprint import pprint
from util.read_properties import getProperties


region=RegionInfo(name='NeCTAR', endpoint='nova.rc.nectar.org.au')
boto_config = getProperties("./boto.properties")

access_key = boto_config['access_key']
secret_key = boto_config['secret_key']
DEFAULT_PLACE = 'melbourne-np'
DEFAULT_IMAGE_ID = 'ami-c163b887'

conn = boto.connect_ec2(aws_access_key_id= access_key,
                        aws_secret_access_key= secret_key, is_secure=True,
                        region=region, port=8773, path='/services/Cloud', validate_certs=False)

images = conn.get_all_images()

img = conn.get_image(DEFAULT_IMAGE_ID)

sec = conn.get_all_security_groups()

sec_group = []

def get_all_instance_ids():
    return map(lambda x: x.instances[0].id, conn.get_all_instances())

def get_instance(instance_id):
    return conn.get_all_instances(instance_id)[0].instances[0]

def terminate_instance(instance_id):
    return get_instance(instance_id).terminate()

def instance_status(instance_id, status, wait_loop = 50):
    init_loop = 0
    while init_loop < wait_loop:
        time.sleep(5)
        if get_instance(instance_id).state == status and get_instance(instance_id).private_ip_address is not None:
            print(get_instance(instance_id).private_ip_address)
            return True
        init_loop = init_loop + 1
    return False

def get_hosts(instances):
    hosts = []
    node = 1
    for i in instances:
        if instance_status(i.id, 'running'):
            hosts.append(get_instance(i.id).private_ip_address)
            name = "Node"+str(node)
            get_instance(i.id).add_tag("Name", name)
            node = node + 1
    print(hosts)
    return hosts

def write_to_hosts(hosts):
    try:
        os.remove('./hosts')
    except OSError as e:
        if e.errno != errno.ENOENT: #no such file or directory
            # re-raise exception if a different error occurred
            raise
    web_host = hosts[0]
    masters = hosts[0]
    node_index = 1
    with open('./hosts', 'a') as f:
        f.write("[webserver]"+"\n")
        f.write("ubuntu@"+web_host+"\n")
        f.write("[masters]"+"\n")
        f.write("node"+str(node_index)+" "+"ansible_ssh_host="+masters+" "+"ansible_user=ubuntu ansible_ssh_private_key_file=/etc/ansible/cloud.key"+"\n")		
        f.write("[cluster]"+"\n")
        for ip in hosts:
            f.write("ubuntu@"+ip+"\n")
        f.write("[slaves]"+"\n")
        for ip in hosts:
            if ip is hosts[0]:
                continue
            else:
                node_index = node_index + 1
                f.write("node"+str(node_index)+" "+"ansible_ssh_host="+ip+" "+"ansible_user=ubuntu ansible_ssh_private_key_file=/etc/ansible/cloud.key"+"\n")
        f.write("[zookeepers:children]"+ "\n" + "masters\n")
        f.write("[spark-masters:children]"+"\n"+"masters\n")
        f.write("[spark-workers:children]"+"\n"+"slaves\n")
    f.close()

def create_instances(instance_number, max_loop = 10):
    instance_list = []
    for c in range(instance_number):
        instance_list.append(
            conn.run_instances(
                DEFAULT_IMAGE_ID,
                key_name='cloud36',
                instance_type='m1.small',
                security_groups=['ssh','http','icmp'],
                placement=DEFAULT_PLACE)
        )

    instances = map(lambda x: x.instances[0], instance_list)
    hosts = get_hosts(instances)
    write_to_hosts(hosts)

if __name__ == '__main__':  
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', type=int, default=1, help='instance count')
    parser.add_argument('-o', type=str, help='start or terminate')
    args = parser.parse_args()  
    if not args.o:
        parser.print_help()
        exit(1)
    if args.o.lower() == 'start':
        create_instances(args.n)
    elif args.o.lower() == 'terminate':
        for ins_id in get_all_instance_ids():
            terminate_instance(ins_id)



